import re
from itertools import zip_longest
import numpy as np
from scipy.spatial.distance import jaccard, hamming, rogerstanimoto, kulsinski, russellrao, dice


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def parse_loss(filename):
    # Compile regular expression
    p = re.compile(r'id\s(\d+)')  # For loss we only need the id of each probe
    # List that will hold the ids of the captured probes
    packet_ids = list()
    with open(filename) as f:
        # Every two lines of the file correspond to a single captured probe
        for line1, line2 in grouper(f, 2):
            # Concatenate  the two lines
            s = line1.rstrip() + ' ' + line2.lstrip()
            # Get probe id and append it to the list
            packet_ids.append(int(p.search(s).group(1)))
    return packet_ids


def parse_jitter(filename):
    # Compile regular expression that extracts probe id
    p = re.compile(r'id\s(\d+)')
    # Dictionary that will hold the ids and the timestamps of the captured probes: {probe_id: timestamp}
    probes = dict()
    with open(filename) as f:
        # Every two lines of the file correspond to a single captured probe
        for line1, line2 in grouper(f, 2):
            # Concatenate  the two lines
            s = line1.rstrip() + ' ' + line2.lstrip()
            # Get probe id
            pid = int(p.search(s).group(1))
            # Get timestamp - with tcpdump captions, timestamps are always at the start of each line
            time = s.split()[0]  # string of the form hh:mm:ss.frac
            h, m, s = time.split(':')
            msecs = (int(h)*3600 + int(m)*60 + float(s)) * \
                1000  # timestamp in microseconds
            probes[pid] = msecs
    return probes


def estimate_loss_distances(src_filename, dst_filenames_dict):
    src_ids = parse_loss(src_filename)  # list of ids of sent probes
    # total number of probes sent (captured in source output interface)
    num_of_probes = len(src_ids)
    # number of nodes is 1 (source) plus number of destinations
    num_of_nodes = len(dst_filenames_dict.keys()) + 1
    # dictionary that holds the list of ids of captured probes for each destination
    dst_ids = dict()
    # Create array of distances
    distances = np.empty((num_of_nodes,)*2)

    # Get list of ids of captured probes for each destination
    for d, f in dst_filenames_dict.items():
        dst_ids[d] = parse_loss(f)

    # Estimate outcome variabes at destination nodes
    Xhat = dict()
    for i in range(1, num_of_nodes):
        Xhat[i] = len(dst_ids[i]) / num_of_probes

    # Calculate array of estimated distances between terminal nodes
    for i in range(0, num_of_nodes):
        for j in range(i, num_of_nodes):
            if i == j:
                # distance between every node and itself
                distances[i, j] = 0
            elif i == 0:
                # distances from source
                distances[i, j] = np.log10(1 / Xhat[j])
                # the array of distances is symmetric
                distances[j, i] = distances[i, j]
            else:
                Xhat_ij = len(set(dst_ids[i]) & set(
                    dst_ids[j])) / num_of_probes
                distances[i, j] = np.log10((Xhat[i] * Xhat[j]) / Xhat_ij**2)
                # the array of distances is symmetric
                distances[j, i] = distances[i, j]
    return distances


def estimate_loss_seq_distances(src_filename, dst_filenames_dict, formula):
    # dictionary that holds different dissimilarity functions
    choices = {'jaccard': jaccard,
               'hamming': hamming,
               'rogerstanimoto': rogerstanimoto,
               'kulsinski': kulsinski,
               'russellrao': russellrao,
               'dice': dice}
    src_ids = parse_loss(src_filename)  # list of ids of sent probes
    # total number of probes sent (captured in source output interface)
    num_of_probes = len(src_ids)
    # number of nodes is 1 (source) plus number of destinations
    num_of_nodes = len(dst_filenames_dict.keys()) + 1
    # dictionary that holds the list of ids of captured probes for each destination
    dst_ids = dict()
    sequences = dict()  # dictionary that holds the bit sequences for each destination
    # Create array of distances
    distances = np.empty((num_of_nodes,)*2)

    # Get list of ids of captured probes for each destination
    for d, f in dst_filenames_dict.items():
        dst_ids[d] = parse_loss(f)

    # Create bit sequences for every node
    # 1 means the respective probe has reached the specific node - 0 means the probe has been lost
    # we assume that all probes have "reached" the source
    sequences[0] = [1]*num_of_probes
    for n in dst_ids.keys():
        sequences[n] = list()
        for probe in src_ids:  # for every probe
            if probe in dst_ids[n]:  # probe has successfully reached node n
                sequences[n].append(1)
            else:  # probe has been lost
                sequences[n].append(0)

    # Calculate array of distances between terminal nodes
    for i in range(0, num_of_nodes):
        for j in range(i, num_of_nodes):
            if i == j:
                # distance between every node and itself
                distances[i, j] = 0
            else:
                # calculate distance between the sequences of nodes i and j
                distances[i, j] = choices[formula](sequences[i], sequences[j])
                # the array of distances is symmetric
                distances[j, i] = distances[i, j]
    return distances, sequences



def estimate_jitter_distances(src_filename, dst_filenames_dict):
    # dictionary of sent probes - id and timestamp
    src_probes = parse_jitter(src_filename)
    # number of nodes is 1 (source) plus number of destinations
    num_of_nodes = len(dst_filenames_dict.keys()) + 1
    # dictionary that holds the captured probes ids and timestamps for each destination (in the form of an inner dictionary)
    dst_probes = dict()
    # Create array of distances
    distances = np.empty((num_of_nodes,)*2)

    # Get list of captured probes for each destination
    for d, f in dst_filenames_dict.items():
        dst_probes[d] = parse_jitter(f)

    # Estimate variance of delays at destination nodes
    Varhat = dict()
    for i in range(1, num_of_nodes):
        # List that holds the delays of the probes
        delays = list()
        # for probes that have not been lost
        for probe_id in dst_probes[i].keys():
            delays.append(dst_probes[i][probe_id] - src_probes[probe_id])
        Varhat[i] = np.var(delays, ddof=1)  # unbiased estimate of variance

    # Calculate array of estimated distances between terminal nodes
    for i in range(0, num_of_nodes):
        for j in range(i, num_of_nodes):
            if i == j:
                # distance between every node and itself
                distances[i, j] = 0
            elif i == 0:
                # distances from source
                distances[i, j] = Varhat[j]
                # the array of distances is symmetric
                distances[j, i] = distances[i, j]
            else:
                delays_i = list()
                delays_j = list()
                # for probes that have not been lost for both nodes i and j
                for probe_id in set(dst_probes[i].keys()) & set(dst_probes[j].keys()):
                    delays_i.append(
                        dst_probes[i][probe_id] - src_probes[probe_id])
                    delays_j.append(
                        dst_probes[j][probe_id] - src_probes[probe_id])
                # unbiased estimate of covariance
                Covhat_ij = np.cov(delays_i, delays_j, ddof=1)[0][1]
                distances[i, j] = Varhat[i] + Varhat[j] - 2*Covhat_ij
                # the array of distances is symmetric
                distances[j, i] = distances[i, j]
    return distances


if __name__ == '__main__':
    ########### TESTING ############
    mat = estimate_jitter_distances('src2.txt', {1: 'dst2.txt'})
    print(mat)
    print(((mat[0][1])**0.5)*1000)
