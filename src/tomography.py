#!/usr/bin/python3 -W ignore
import argparse
import pickle
import distances
import reciprocal_lca
import math


def main():
    parser = argparse.ArgumentParser(description='Logical routing tree inference and link performance parameters estimation.')
    parser.add_argument('-v', '--verbose', action='store_true', help='increase output verbosity' )
    parser.add_argument('-t', '--type', required=True, choices=['loss', 'jitter', 'loss-sequences'], help='specify type of performance parameter')
    parser.add_argument('-r', '--reduction-formula', choices=['midpoint', 'maximal'], default='midpoint', help='choose formula employed in the LCA matrix reduction')
    parser.add_argument('-d', '--dissimilarity', choices=['jaccard', 'hamming', 'rogerstanimoto', 'dice'], default='jaccard', help='choose dissimilarity measure for binary sequences')
    parser.add_argument('-sf', '--src-file', required=True, help='specify capture text file of source node')
    parser.add_argument('-df', '--dst-file', required=True, nargs='+', help='specify capture text file(s) of destination node(s)')
    parser.add_argument('-i', '--image', default='topology.png', help='save inferred logical routing tree to image')
    parser.add_argument('-o', '--output', default='routing_tree.bin', help='save inferred tree object to binary file')
    parser.add_argument('-c', '--threshold', default=1e-9, help='choose threshold used in tree pruning')
    args = parser.parse_args()
    #print(args)

    if args.type == 'loss':
        # source ID is assumed 0 and the IDs of the destination nodes follow in ascending order
        dist_mat = distances.estimate_loss_distances(args.src_file, {k+1: v for k, v in enumerate(args.dst_file)})
    elif args.type == 'jitter':
        dist_mat = distances.estimate_jitter_distances(args.src_file, {k+1: v for k, v in enumerate(args.dst_file)})
    else:
        dist_mat, seq_dict = distances.estimate_loss_seq_distances(args.src_file, {k+1: v for k, v in enumerate(args.dst_file)}, args.dissimilarity)

    if args.verbose:
        print('\nMatrix of distances between terminal nodes:\n')
        print(dist_mat)

    threshold = float(args.threshold)
    if args.type == 'loss-sequences':
        tree = reciprocal_lca.rec_lca_seq(dist_mat, seq_dict, threshold, formula=args.dissimilarity, verbose=args.verbose)
    else:
        tree = reciprocal_lca.rec_lca(dist_mat, threshold, formula=args.reduction_formula, verbose=args.verbose)

    # save topology to image
    tree.draw(args.image)

    # save to binary file
    with open(args.output, 'wb') as f:
        # Pickle the tree object
        pickle.dump(tree, f)

    if args.verbose:
        print('\nLogical Routing Tree:')
        print('\nNodes:', [n.get_ID() for n in tree.get_nodes()])
        if args.type == 'jitter':
            for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
                print('Edge: {} --- {} [Length: {:.4f}] [Jitter (ms): {:.3f}]'.format(s.get_ID(), r.get_ID(), l, math.sqrt(l)))
        elif args.type == 'loss':
            for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
                print('Edge: {} --- {} [Length: {:.4f}] [Loss Rate (%): {:.3%}]'.format(s.get_ID(), r.get_ID(), l, 1-10**(-l)))
        else:
            for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
                print('Edge: {} --- {} [Length: {:.4f}]'.format(s.get_ID(), r.get_ID(), l))
                
    print()

    
if __name__ == '__main__':
    main()