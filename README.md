# LCA-RNN clustering algorithm

An agglomerative clustering algorithm for multicast routing tree inference and link characteristics estimation that incorporates a nearest neighbors (NN) chain and iteratively joins pairs of reciprocal nearest neighbors (RNNs) in terms of distance from source to the lowest common ancestor (LCA) of the nodes. For more details see:

G. Kakkavas, V. Karyotis and S. Papavassiliou, "A Distance-based Agglomerative Clustering Algorithm for Multicast Network Tomography," *ICC 2020 - 2020 IEEE International Conference on Communications (ICC)*, 2020, pp. 1-7, doi: [10.1109/ICC40277.2020.9149412](https://ieeexplore.ieee.org/abstract/document/9149412).

## Prerequisites

This project has been developed and tested in Python 3.7.4 and requires the following libraries:

- NumPy 1.17.0
- SciPy 1.3.1
- Matplotlib 3.1.1
- NetworkX 2.3
- pydot 1.4.1

## Usage

The tcpdump capture text files must be produced with the verbose option (-vvv). The source ID is assumed 0 and the IDs of the destination nodes follow in ascending order.

```
usage: tomography.py [-h] [-v] -t {loss,jitter,loss-sequences}
                     [-r {midpoint,maximal}]
                     [-d {jaccard,hamming,rogerstanimoto,dice}] -sf SRC_FILE
                     -df DST_FILE [DST_FILE ...] [-i IMAGE] [-o OUTPUT]
                     [-c THRESHOLD]

Logical routing tree inference and link performance parameters estimation.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -t {loss,jitter,loss-sequences}, --type {loss,jitter,loss-sequences}
                        specify type of performance parameter
  -r {midpoint,maximal}, --reduction-formula {midpoint,maximal}
                        choose formula employed in the LCA matrix reduction
  -d {jaccard,hamming,rogerstanimoto,dice}, --dissimilarity {jaccard,hamming,rogerstanimoto,dice}
                        choose dissimilarity measure for binary sequences
  -sf SRC_FILE, --src-file SRC_FILE
                        specify capture text file of source node
  -df DST_FILE [DST_FILE ...], --dst-file DST_FILE [DST_FILE ...]
                        specify capture text file(s) of destination node(s)
  -i IMAGE, --image IMAGE
                        save inferred logical routing tree to image
  -o OUTPUT, --output OUTPUT
                        save inferred tree object to binary file
  -c THRESHOLD, --threshold THRESHOLD
                        choose threshold used in tree pruning
```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
